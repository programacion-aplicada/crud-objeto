package com.springboot.objetos.models.dao;

import com.springboot.objetos.models.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface IClienteDao extends CrudRepository<Cliente, Long>{

}
